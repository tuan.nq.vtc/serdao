<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DataLengthValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        $combinedValue = $value['firstname'] . $value['lastname'] . $value['address'];

        if (mb_strlen($combinedValue) > $constraint->limit) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ limit }}', $constraint->limit)
                ->addViolation();
        }
    }
}
