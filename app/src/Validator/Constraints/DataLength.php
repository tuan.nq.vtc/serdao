<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DataLength extends Constraint {

    public string $message = 'Combined value cannot be longer than {{ limit }} characters';
    public int $limit = 255;
}