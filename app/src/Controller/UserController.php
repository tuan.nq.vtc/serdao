<?php

namespace App\Controller;

use App\DTO\UserDTO;
use App\FormType\UserForm;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{

    public function __construct(protected UserService $service)
    {
    }

    #[Route('/user', name: 'handle_user_creating', methods: ['POST'])]
    public function create(Request $request): Response
    {
        $form = $this->createForm(UserForm::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $this->service->create(new UserDTO(
                firstName: $data['firstname'] ?? '', lastName: $data['lastname'] ?? '', address: $data['address']
            ));
        }

        return $this->render('user.html.twig', [
            'form' => $form->createView(),
            'obj' => $request->getMethod(),
            'users' => $this->service->getAll()
        ]);
    }

    #[Route(path: '/user', name: 'handle_user_listing',methods: ['GET'], )]
    public function request(Request $request): Response
    {
        return $this->render('user.html.twig', [
            'form' => $this->createForm(UserForm::class)->createView(),
            'obj' => $request->getMethod(),
            'users' => $this->service->getAll()
        ]);
    }

    #[Route(path: '/user/delete/{id}', name: 'handle_user_deleting',methods: ['GET'], )]
    public function delete($id) {
        $user = $this->service->findById($id);
        if (!$user) {
            $this->addFlash('notice', 'User is not found');
        }

        $this->service->delete($user);
        return $this->redirect('/user');
    }

}