<?php

namespace App\Service;

use App\DTO\UserDTO;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class UserService
{

    protected EntityRepository $userRepository;

    public function __construct(protected EntityManagerInterface $userManager)
    {
        $this->userRepository = $this->userManager->getRepository(User::class);
    }

    public function getAll(): array
    {
        return $this->userRepository->findAll();
    }

    public function create(UserDTO $dto): User
    {
        $user = $dto->transformUserData();
        $this->userManager->persist($user);
        $this->userManager->flush();

        return $user;
    }

    public function findById($id)
    {
        return $this->userRepository->find($id);
    }

    public function delete(User $user): void
    {
        $this->userRepository->delete($user);
    }
}