<?php

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;

class UserSeeder extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $manager->persist($this->createUserSample('Barack - Obama - White House'));
        $manager->persist($this->createUserSample('Britney - Spears - America'));
        $manager->persist($this->createUserSample('Leonardo - DiCaprio - Titanic'));

        $manager->flush();
    }

    private function createUserSample(string $data): User
    {
        return (new User())->setData($data);
    }
}